//
//  GameScene.swift
//  spinner
//
//  Created by Shithin PV on 11/11/18.
//  Copyright © 2018 e3Help. All rights reserved.
//

import SpriteKit

class GameScene: SKScene{
    
    private var spinWheelOpen = false
    private var spinWheel:SpinWheel!
    
    override func didMove(to view: SKView) {
        setupSpinWheel()
        spinWheelOpen = true
    }
    
    
    private func setupSpinWheel(){
        spinWheel = SpinWheel(size: self.size)
        spinWheel.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        spinWheel.zPosition = 500
        addChild(spinWheel)
    }
}
