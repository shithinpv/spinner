//
//  SpinWheel.swift
//  spinner
//
//  Created by Shithin PV on 18/11/18.
//  Copyright © 2018 e3Help. All rights reserved.
//

import Foundation
import SpriteKit

enum WheelState : Int {
    case stopped
    case ready
    case spinning
    case waiting
}

class SpinWheel: SKSpriteNode{
    
    var wheelRim: SKSpriteNode!
    var cake: SKNode!
    var wheelHub: PushButton!
    var wheelState: WheelState = .waiting
    var wooshSound: SKAction!
    
    
    let colors = [SKColor.black, SKColor.red, SKColor.blue, SKColor.purple, SKColor.black, SKColor.red]
    var shadesOfCake:[SKColor] = []
    
    init(size:CGSize){
        super.init(texture: nil, color: UIColor.clear, size: size)
        self.size = size
        shadesOfCake = [SKColor(red: 104/255, green: 225/255, blue: 134/255, alpha: 1),
                        SKColor(red: 70/255, green: 156/255, blue: 140/255, alpha: 1),
                        SKColor(red: 139/255, green: 252/255, blue: 204/255, alpha: 1),
                        SKColor(red: 30/255, green: 74/255, blue: 73/255, alpha: 1),
                        SKColor(red: 92/255, green: 202/255, blue: 200/255, alpha: 1),
                        SKColor(red: 85/255, green: 185/255, blue: 152/255, alpha: 1)
                        ]
        setupWheel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupWheel(){
        wooshSound = SKAction.playSoundFileNamed("woosh.aac", waitForCompletion: false)
        
        wheelRim = SKSpriteNode(imageNamed: "wheel.png")
        wheelRim.position = CGPoint(x: 0, y: 0)
        wheelRim.name = "wheelRim"
        wheelRim.zPosition = 500
        addChild(wheelRim)
        
        getCakePieces(number: 6)
        
        wheelHub = PushButton(upImage: "wheel_hub", downImage: "wheel_hub")
        wheelHub.setButtonAction(target: self, event: .touchUpInside, function: spinWheel, parent: self)
        wheelHub.zPosition = 1000
        wheelHub.position = wheelRim.position
        wheelHub.size = CGSize(width: 50, height: 50)
        wheelHub.bounce = false
        wheelHub.physicsBody = SKPhysicsBody(circleOfRadius: wheelHub.size.width / 2)
        wheelHub.physicsBody!.isDynamic = false
        addChild(wheelHub)
    }
    
    func spinWheel() {
        
        if kHubSpinsWheel && wheelState != .spinning {
            let spinPower = CGFloat.random(min: 50, max: 100)
            spin(spinPower)
            wheelState = .spinning
        }
    }
    
    func spin(_ impulse: CGFloat) {
        
        //print("velocity \(impulse)");
        cake.physicsBody!.applyAngularImpulse(-(impulse * 30.0))
        cake.physicsBody!.angularDamping = 1
        let maxAngularVelocity: CGFloat = 80
        cake.physicsBody!.angularVelocity = cake.physicsBody!.angularVelocity//min(circle.physicsBody!.angularVelocity, maxAngularVelocity)
        
        run(wooshSound)
    }
    
    
    func getCakePieces(number:Int){
        let pieceAngle = (2*CGFloat.pi)/CGFloat(number)
        let width = size.width/2-30
        cake = SKNode()
        for pieceIndex in 0 ..< number{
            let piecePath = UIBezierPath()
            piecePath.move(to: CGPoint(x: 0, y: 0))
            let startAngle = CGFloat(pieceIndex)*pieceAngle
            let endAngle = CGFloat(pieceIndex+1)*pieceAngle
            piecePath.addArc(withCenter: CGPoint.zero,
                         radius: width,
                         startAngle: startAngle,
                         endAngle: endAngle,
                         clockwise: true)
            let singleCakeNode = SKShapeNode(path: piecePath.cgPath)
            singleCakeNode.fillColor = shadesOfCake[pieceIndex % shadesOfCake.count]
            singleCakeNode.strokeColor = SKColor.white
            singleCakeNode.lineWidth = 5
            cake.addChild(singleCakeNode)
        }
        let cakeBody = SKPhysicsBody(circleOfRadius: width)
        cake.zPosition = 100
        cakeBody.isDynamic = true
        cakeBody.collisionBitMask = 0
        cakeBody.affectedByGravity = false
        cakeBody.mass = 100
        cake.physicsBody = cakeBody
        addChild(cake)
    }
}
