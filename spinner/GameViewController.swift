//
//  GameViewController.swift
//  spinner
//
//  Created by Shithin PV on 11/11/18.
//  Copyright © 2018 e3Help. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController:UIViewController{
    @IBOutlet var skView: SKView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = GameScene(size: skView.bounds.size)
        scene.scaleMode = .aspectFill
        scene.position = CGPoint(x: skView.frame.midX, y: skView.frame.midY)
        skView.presentScene(scene)
        skView.ignoresSiblingOrder = true
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.showsPhysics = true
    }
}
